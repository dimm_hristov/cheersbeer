DROP SCHEMA IF EXISTS `beertag` ;
CREATE SCHEMA IF NOT EXISTS `beertag` DEFAULT CHARACTER SET latin1 ;
USE `beertag` ;

create table breweries
(
	brewery_id int auto_increment
		primary key,
	brewery_name varchar(55) not null
);

create table origincountries
(
	country_id int auto_increment
		primary key,
	country_name varchar(32) not null,
	constraint origincountries_CountryName_uindex
		unique (country_name)
);

create table roles
(
	role_id int auto_increment
		primary key,
	role varchar(15) not null,
	constraint roles_Role_uindex
		unique (role)
);

create table styles
(
	style_id int auto_increment
		primary key,
	style varchar(50) null
);

create table tags
(
	id int auto_increment
		primary key,
	tag varchar(30) null,
	constraint tags_tag_uindex
		unique (tag)
);

create table users
(
	user_id int auto_increment
		primary key,
	username varchar(20) not null,
	firstname varchar(20) not null,
	lastname varchar(20) not null,
	email varchar(100) charset utf8 not null,
	profilepicture blob null,
	role_id int not null,
	password varchar(60) charset utf8 not null,
	constraint users_Email_uindex
		unique (email),
	constraint users_username_uindex
		unique (username),
	constraint users_roles_RoleID_fk
		foreign key (role_id) references roles (role_id)
);

create table beers
(
	beer_id int auto_increment
		primary key,
	beer_name varchar(30) not null,
	description mediumtext not null,
	origincountry_id int not null,
	brewery_id int not null,
	style_id int not null,
	alcoholbyvolume double null,
	images blob null,
	creator_id int not null,
	constraint beers_breweries_brewery_id_fk
		foreign key (brewery_id) references breweries (brewery_id),
	constraint beers_origincountries_CountryID_fk
		foreign key (origincountry_id) references origincountries (country_id),
	constraint beers_styles_style_id_fk
		foreign key (style_id) references styles (style_id),
	constraint beers_users_user_id_fk
		foreign key (creator_id) references users (user_id)
);

create index beers_alcoholbyvolume_abvID_fk
	on beers (alcoholbyvolume);

create table drunkbeers
(
	user_id int not null,
	beer_id int not null,
	rating double null,
	constraint drunkbeers_beers_BeerID_fk
		foreign key (beer_id) references beers (beer_id),
	constraint drunkbeers_users_UserID_fk
		foreign key (user_id) references users (user_id)
);

create table taggedbeers
(
	id int auto_increment
		primary key,
	tag_id int not null,
	beer_id int not null,
	constraint taggedbeers_beers_BeerID_fk
		foreign key (beer_id) references beers (beer_id),
	constraint taggedbeers_tags_id_fk
		foreign key (tag_id) references tags (id)
);

create table wishtodrink
(
	user_id int not null,
	beer_id int not null,
	constraint wishtodrink_beers_BeerID_fk
		foreign key (beer_id) references beers (beer_id),
	constraint wishtodrink_users_UserID_fk
		foreign key (user_id) references users (user_id)
);

