package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.models.drunkbeers.DrunkBeers;
import com.example.beertag.models.drunkbeers.DrunkBeersId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository {

    void rate(User user, Beer beer, int rating);

    double calculateRating(Beer beer);

    String printAvgRating(Beer beer);

    void unrateBeer(User user, Beer beer);
}
