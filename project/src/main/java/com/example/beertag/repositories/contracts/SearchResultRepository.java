package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Beer;

import java.util.List;

public interface SearchResultRepository {
    List<Beer> getSearchResult();

    void setSearchResult(List<Beer> searchResult);

    String getLastSortedBy();

    void setLastSortedBy(String lastSortedBy);
}
