package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Brewery;
import com.example.beertag.models.Style;

import java.util.List;

public interface BreweryRepository {

    List<Brewery> getAll();

    Brewery getById(int id);

    Brewery getByBreweryName(String name);

    void create(Brewery brewery);

    void update(Brewery brewery);

    void delete(int id);
}
