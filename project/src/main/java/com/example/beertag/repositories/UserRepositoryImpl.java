package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with id %d doesn't exist", id));
            }
            if (user.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        ("Access denied. User %s has been deleted", user.getUserName()));
            }
            return user;
        }
    }

    @Override
    public List<User> filterByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where userName like concat('%',:username,'%')", User.class);
            query.setParameter("username", username);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list().stream().filter(u -> !u.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<User> getAllDeletedUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list().stream().filter(User::isDeleted).collect(Collectors.toList());
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            User userToDelete = session.get(User.class, id);
            if (userToDelete.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format("User %s is already deleted",
                        userToDelete.getUserName()));
            }
            userToDelete.setDeleted(true);
            session.beginTransaction();
            session.update(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Set<Beer> getDrunkenBeers(User user) {
        return user.getDrunkenBeers().stream().filter(b->!b.isDeleted()).collect(Collectors.toSet());
    }

    @Override
    public Set<Beer> getDrunkenBeers(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            return user.getDrunkenBeers().stream().filter(b->!b.isDeleted()).collect(Collectors.toSet());
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where userName = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            User user = users.get(0);
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with username %s doesn't exist", username));
            }
            if (user.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        ("Access denied. User %s has been deleted", user.getUserName()));
            }
            return users.get(0);
        }
    }

    @Override
    public Set<Beer> getWishList(User user) {
        return user.getWishList().stream().filter(b->!b.isDeleted()).collect(Collectors.toSet());
    }

    @Override
    public Set<Beer> getWishList(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            return user.getWishList().stream().filter(b->!b.isDeleted()).collect(Collectors.toSet());
        }
    }

    @Override
    public void removeFromList(String list, int userId, int beerId) {
        User userFromRepo;
        try (Session session = sessionFactory.openSession()) {
            userFromRepo = session.get(User.class, userId);
            session.beginTransaction();

            switch (list) {
                case "wishlist":
                    Set<Beer> set = userFromRepo.getWishList();
                    set.removeIf(beer1 -> beer1.getId() == beerId);

                    break;
                case "drunklist":
                    Set<Beer> setDrunken = userFromRepo.getDrunkenBeers();
                    setDrunken.removeIf(beer1 -> beer1.getId() == beerId);
                    break;
            }
            session.update(userFromRepo);
            session.getTransaction().commit();
        }
    }
}


