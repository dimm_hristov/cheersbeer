package com.example.beertag.repositories;

import com.example.beertag.models.OriginCountry;
import com.example.beertag.repositories.contracts.OriginCountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class OriginCountryImpl  implements OriginCountryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OriginCountryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<OriginCountry> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<OriginCountry> query = session.createQuery("from OriginCountry ", OriginCountry.class);
            return query.list();
        }
    }

    @Override
    public OriginCountry getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            OriginCountry originCountry = session.get(OriginCountry.class, id);
            if (originCountry == null) {
                throw new EntityNotFoundException(String.format("Country with id %d doesn't exist", id));
            }
            return originCountry;
        }
    }
    @Override
    public OriginCountry getByCountryName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<OriginCountry> query = session.createQuery("from OriginCountry where country = :name", OriginCountry.class);
            query.setParameter("name", name);
            return query.list().get(0);

        }
    }

    @Override
    public void create(OriginCountry originCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(originCountry);
        }
    }

    @Override
    public void update(OriginCountry originCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(originCountry);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            OriginCountry countryToDelete = session.get(OriginCountry.class, id);
            session.beginTransaction();
            session.delete(countryToDelete);
            session.getTransaction().commit();
        }
    }
}
