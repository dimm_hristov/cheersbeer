package com.example.beertag.repositories;

import com.example.beertag.repositories.contracts.CommonRepository;

import java.util.List;

public class CommonRepositoryImpl<T> implements CommonRepository<T> {
    @Override
    public List<T> getAll() {
        return null;
    }

    @Override
    public T getById(int id) {
        return null;
    }

    @Override
    public void create(T t) {
    }

    @Override
    public void update(T t) {
    }

    @Override
    public void delete(int id) {
    }
}
