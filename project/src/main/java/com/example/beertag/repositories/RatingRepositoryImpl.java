package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.models.drunkbeers.DrunkBeers;
import com.example.beertag.models.drunkbeers.DrunkBeersId;
import com.example.beertag.repositories.contracts.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void rate(User user, Beer beer, int rating) {
        DrunkBeersId id = new DrunkBeersId(user.getId(), beer.getId());
        DrunkBeers ratingToAdd = new DrunkBeers();
        ratingToAdd.setId(id);
        ratingToAdd.setRating(rating);

        Map<DrunkBeersId, Integer> drunkenBeerMap = getAllUserAndBeerId();

        if (drunkenBeerMap.containsKey(id)) {
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.update(ratingToAdd);
                session.getTransaction().commit();
            }
        } else {
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.save(ratingToAdd);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public double calculateRating(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            int desiredBeerId = beer.getId();
            Query<DrunkBeers> query = session
                    .createQuery("from DrunkBeers d where d.id.beerId =" + desiredBeerId, DrunkBeers.class);
            int totalCount = query.list().size();
            double sumRating = 0;
            for (DrunkBeers drunkBeers : query.list()) {
                sumRating += drunkBeers.getRating();
            }
            return sumRating / totalCount;
        }
    }

    @Override
    public String printAvgRating(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            int desiredBeerId = beer.getId();
            Query<DrunkBeers> query = session
                    .createQuery("from DrunkBeers d where d.id.beerId =" + desiredBeerId, DrunkBeers.class);
            int totalCount = query.list().size();
            double sumRating = 0;
            for (DrunkBeers drunkBeers : query.list()) {
                sumRating += drunkBeers.getRating();
            }
            double result = sumRating / totalCount;
            return String.format("%.2f",result);
        }
    }

    @Override
    public void unrateBeer(User user, Beer beer) {
        DrunkBeersId drunkBeerId = new DrunkBeersId(user.getId(), beer.getId());
        Map<DrunkBeersId, Integer> drunkenBeerMap = getAllUserAndBeerId();

        if (drunkenBeerMap.containsKey(drunkBeerId)) {
            DrunkBeers drunkBeerToDelete = new DrunkBeers(drunkBeerId, drunkenBeerMap.get(drunkBeerId));
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.delete(drunkBeerToDelete);
                session.getTransaction().commit();
            }
        } else {
            throw new EntityNotFoundException("This user has not provided rating");
        }
    }

    private Map<DrunkBeersId, Integer> getAllUserAndBeerId() {
        Session session = sessionFactory.openSession();
        Query<DrunkBeers> query = session.createQuery("from DrunkBeers", DrunkBeers.class);
        List<DrunkBeers> drunkBeers = query.list();
        Map<DrunkBeersId, Integer> mapToReturn = new HashMap<>();
        for (DrunkBeers drunkBeer : drunkBeers) {
            mapToReturn.put(drunkBeer.getId(), drunkBeer.getRating());
        }
        return mapToReturn;
    }
}
