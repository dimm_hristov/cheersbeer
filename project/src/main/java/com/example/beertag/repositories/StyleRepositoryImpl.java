package com.example.beertag.repositories;

import com.example.beertag.models.Style;
import com.example.beertag.repositories.contracts.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory factory) {
        this.sessionFactory = factory;
    }

    @Override
    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style",Style.class);
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException(String.format("Style with id %d doesn't exist", id));
            }
            return style;
        }
    }

    @Override
    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
    }

    @Override
    public void update(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style styleToDelete = session.get(Style.class, id);
            session.beginTransaction();
            session.delete(styleToDelete);
            session.getTransaction().commit();
        }
    }
}
