package com.example.beertag.models.drunkbeers;

import javax.persistence.*;

@Entity
@Table(name="drunkbeers")
public class DrunkBeers {

    @EmbeddedId
    private DrunkBeersId id;

    @Column(name="rating")
    private int rating;

    public DrunkBeers() {
    }

    public DrunkBeers(DrunkBeersId id, int rating) {
        this.id = id;
        this.rating = rating;
    }

    public DrunkBeersId getId() {
        return id;
    }

    public void setId(DrunkBeersId id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
