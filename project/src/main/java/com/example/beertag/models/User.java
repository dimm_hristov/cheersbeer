package com.example.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "user_details")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Size(min = 5, max = 30, message = "Name should be between 5 and 30 symbols")
    @Column(name = "username")
    private String userName;

    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols")
    @Column(name = "firstname")
    private String firstName;

    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols")
    @Column(name = "lastname")
    private String lastName;

    @Email(message = "Please provide a valid email address", regexp = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$")
    @Column(name = "email")
    private String email;

    @Column(name = "profilepicture")
    private String profilePicture;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "drunkbeers",
              joinColumns = @JoinColumn(name = "user_id"),
              inverseJoinColumns = @JoinColumn(name ="beer_id"))
    private Set<Beer> drunkenBeers;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "wishtodrink",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name ="beer_id"))
    private Set<Beer> wishList;

    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Column(name = "deleted")
    private boolean deleted;

//    @Formula(
//            "SELECT authority FROM users u JOIN authorities a on u.username = a.username where u.username = userName"
//    )
//    private String authority;

    public User() {
        setProfilePicture("/images/defaultAvatar.jpg");
    }


    public int getId() {
        return this.id;
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getPassword() {
        return password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Beer> getWishList() {
        return wishList;
    }

    public void setWishList(Set<Beer> wishList) {
        this.wishList = wishList;
    }

    public Set<Beer> getDrunkenBeers() {
        return drunkenBeers;
    }

    public void setDrunkenBeers(Set<Beer> drunkenBeers) {
        this.drunkenBeers = drunkenBeers;
    }

    public void addBeerToDrunkenList(Beer beer) {
        this.drunkenBeers.add(beer);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Integer> getWishListIdsOnly() {
        return this.wishList.stream().map(b ->b.getId()).collect(Collectors.toSet());
    }

    public Set<Integer> getDrunkListIdsOnly() {
        return this.drunkenBeers.stream().map(b ->b.getId()).collect(Collectors.toSet());
    }

//    public String getAuthority() {
//        return authority;
//    }
//
//    public void setAuthority(String authority) {
//        this.authority = authority;
//    }
}
