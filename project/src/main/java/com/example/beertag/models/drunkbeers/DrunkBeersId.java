package com.example.beertag.models.drunkbeers;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

// should it be embeddable ? and why it cant find the user Id and beer Id even though its working
@Embeddable
public class DrunkBeersId implements Serializable {
    @Column(name = "user_id")
    private int userId;
    @Column(name = "beer_id")
    private int beerId;

    public DrunkBeersId() {

    }

    public DrunkBeersId(int userId, int beerId) {
        this.userId = userId;
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DrunkBeersId)) return false;
        DrunkBeersId that = (DrunkBeersId) o;
        return Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getBeerId(), that.getBeerId());

    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getBeerId());
    }
}
