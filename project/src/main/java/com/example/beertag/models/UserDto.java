package com.example.beertag.models;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.io.File;


public class UserDto {

    public static final String DEFAULT_PROFILE_PICTURE = "/images/defaultAvatar.jpg";
    @Positive
    private int id;

    @Size(min = 5, max = 30, message = "Name should be between 5 and 30 symbols")
    private String userName;

    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols")
    private String firstName;

    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols")
    private String lastName;

    @Email(message = "Please provide a valid email address", regexp = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$")
    private String email;

    private String profilePicture;

    public UserDto() {
        setProfilePicture(DEFAULT_PROFILE_PICTURE);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static String getDefaultProfilePicture() {
        return DEFAULT_PROFILE_PICTURE;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
