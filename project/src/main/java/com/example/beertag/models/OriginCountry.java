package com.example.beertag.models;

import javax.persistence.*;

@Entity
@Table(name="origincountries")
public class OriginCountry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="country_id")
    private int id;

    @Column(name="country_name")
    private String country;

    public OriginCountry() {
    }

    public OriginCountry(String country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
