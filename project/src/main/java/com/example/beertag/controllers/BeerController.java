package com.example.beertag.controllers;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.exceptions.InvalidOperationException;
import com.example.beertag.models.*;
import com.example.beertag.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/beers")
public class BeerController {

    private final BeerService beerService;
    private final StyleService styleService;
    private final OriginCountryService originCountryService;
    private final BreweryService breweryService;
    private final TagService tagService;
    private final UserService userService;
    private final RatingService ratingService;
    private final SearchResultService searchResultService;

    @Autowired
    public BeerController(BeerService beerService, StyleService styleService,
                          OriginCountryService originCountryService, BreweryService breweryService,
                          TagService tagService, UserService userService, RatingService ratingService,
                          SearchResultService searchResultService) {
        this.beerService = beerService;
        this.styleService = styleService;
        this.originCountryService = originCountryService;
        this.breweryService = breweryService;
        this.tagService = tagService;
        this.userService = userService;
        this.ratingService = ratingService;
        this.searchResultService = searchResultService;
    }

    @GetMapping
    public String showAllBeersPage(Model model, Principal principal) {
        User user = mapFromPrincipalToUser(principal);
        // for displaying information
        searchResultService.addBeers(beerService.getAll());
        model.addAttribute("beers", searchResultService.getList());
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        // for receiving user input
        model.addAttribute("searchBeerDto", new SearchBeerDto());
        model.addAttribute("user", user);
        return "beers_page";
    }

    @PostMapping
    public String searchForBeers(@ModelAttribute SearchBeerDto dto, Model model, Principal principal) {
        User user = mapFromPrincipalToUser(principal);
        searchResultService.addBeers(beerService.searchByCriteria
                (dto.getName(), dto.getStyleId(), dto.getCountryId(), dto.getTags()));

        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("beers", searchResultService.getList());
        model.addAttribute("searchBeerDto", new SearchBeerDto());
        model.addAttribute("user", user);
        return "beers_page";
    }


    @GetMapping("/sort")
    public String sort(Model model, Principal principal, @RequestParam String filter) {
        switch (filter) {
            case "name":
                searchResultService.setLastSortedBy("name");
                break;
            case "abv":
                searchResultService.setLastSortedBy("abv");
                break;
            case "rating":
                searchResultService.setLastSortedBy("rating");
                break;
        }
        User user = mapFromPrincipalToUser(principal);
        model.addAttribute("user", user);
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("searchBeerDto", new SearchBeerDto());
        List<Beer> beerResult = searchResultService.getList();
        beerService.sort(filter, beerResult);
        model.addAttribute("beers", beerResult);
        return "beers_page";
    }

    @GetMapping("/sort/by")
    public String sortAscDesc(Model model, Principal principal, @RequestParam String filter) {
        User user = mapFromPrincipalToUser(principal);
        model.addAttribute("user", user);
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("searchBeerDto", new SearchBeerDto());
        List<Beer> beerResult = searchResultService.getList();
        if (filter.equals("asc")) {
            switch (searchResultService.getLastSortedBy()) {
                case "name":
                    beerResult.sort(Comparator.comparing(Beer::getName));
                    model.addAttribute("beers", beerResult);
                    break;
                case "abv":
                    beerResult.sort(Comparator.comparing(Beer::getAbv));
                    model.addAttribute("beers", beerResult);
                    break;
                case "rating":
                    beerResult.sort(Comparator.comparing(Beer::getRating));
                    model.addAttribute("beers", beerResult);
                    break;
            }
        } else if (filter.equals("dsc")) {
            switch (searchResultService.getLastSortedBy()) {
                case "name":
                    beerResult.sort(Comparator.comparing(Beer::getName).reversed());
                    model.addAttribute("beers", beerResult);
                    break;
                case "abv":
                    beerResult.sort(Comparator.comparing(Beer::getAbv).reversed());
                    model.addAttribute("beers", beerResult);
                    break;
                case "rating":
                    beerResult.sort(Comparator.comparing(Beer::getRating).reversed());
                    model.addAttribute("beers", beerResult);
                    break;
            }
        }
        return "beers_page";
    }

    @PostMapping
    @RequestMapping("/{beerId}/info")
    public String getBeerInfo(Model model, @PathVariable int beerId, Principal principal) {
        model.addAttribute("beer", beerService.getById(beerId));
        model.addAttribute("avgBeerRating", ratingService.printAvgRating(beerId));
        if (principal != null) {

            User loggedUser = userService.getByUsername(principal.getName());
            User creator = beerService.getById(beerId).getCreator();
            model.addAttribute("user", loggedUser);
            model.addAttribute("creator", creator);
            int a = 2;
        } else {
            model.addAttribute("user", beerService.getById(beerId));
            model.addAttribute("creator", beerService.getById(beerId));
        }

        return "beer_info";
    }

    @PostMapping("/rate/{beerId}")
    public String rateBeer(@PathVariable int beerId, Principal principal, @RequestParam int filter, Model model) {
        try {
            ratingService.rate(principal.getName(), beerId, filter);
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            model.addAttribute("ratingError", e.getMessage());
        }
        return "redirect:/beers/" + beerId + "/info";
    }

    @GetMapping("/new")
    public String getCreateNewBeer(Model model, Principal principal) {

        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);
        model.addAttribute("createBeerDto", new CreateBeerDto());
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());

        return "create_beer";

    }

    @PostMapping("/new")
    public String createNewBeer(@Valid @ModelAttribute CreateBeerDto beerDto,
                                BindingResult bindingResult, Model model, Principal principal) {
        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);

        model.addAttribute("createBeerDto", beerDto);
        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());

        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingError", bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.joining(" ")));
            System.out.println(bindingResult.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.joining(" ")));
            return "create_beer";
        }


        Beer beer = new Beer();
        User user = userService.getByUsername(principal.getName());
        try {
            beer = mapper.toBeerFromCreateBeerDto(beerDto, beer, user, styleService, beerService,
                    originCountryService, breweryService, tagService);
            userService.addBeerToUserDrunkenList(beer.getId(), user.getUserName());
            ratingService.rate(user.getUserName(), beer.getId(), 0);
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            System.out.println(e.getMessage());
            return "create_beer";
        }

        return "redirect:/beers";

    }

    @GetMapping("/{id}/")
    public String getEditPage(@PathVariable Integer id, Model model, Principal principal) {
        Beer beer;
        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);

        try {
            beer = beerService.getById(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
        CreateBeerDto dto = new CreateBeerDto();
        dto.setName(beer.getName());
        dto.setAbv(beer.getAbv());
        dto.setBrewery(beer.getBrewery().getBrewery());
        dto.setStyleId(beer.getStyle().getId());
        dto.setDescription(beer.getDescription());
        dto.setCountryId(beer.getOriginCountry().getId());
        dto.setPicture(beer.getPicture());
        StringBuilder stringTags = new StringBuilder();
        beer.getTags().stream().map(Tag::getTag).forEach(t -> {
            stringTags.append(t);
            stringTags.append(" ");
        });
        dto.setHashtags(stringTags.toString().trim());

        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("createBeerDto", dto);
        model.addAttribute("beer", beer);

        return "create_beer";
    }


    @PostMapping("/{id}/")
    public String handleEditBeer(@PathVariable int id, @Valid @ModelAttribute CreateBeerDto dto,
                                 BindingResult bindingResult, Model model, Principal principal) {
        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);
        Beer beer;
        try {
            beer = beerService.getById(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }

        model.addAttribute("styles", styleService.getAll());
        model.addAttribute("countries", originCountryService.getAll());
        model.addAttribute("createBeerDto", dto);

        model.addAttribute("beer", beer);

        if (bindingResult.hasErrors()) {
            return "create_beer";
        }
        User user = userService.getByUsername(principal.getName());
        try {
            beer = mapper.editBeerFromDto(dto, beer, user, styleService, beerService,
                    originCountryService, breweryService, tagService);
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        beerService.update(beer, user);
        return "redirect:/beers";

    }

    @PostMapping("/add/toWishList/{beerId}")
    public String addToWishList(@PathVariable int beerId, Principal principal, Model model) {
        try {
            userService.addBeerToUserWishList(beerId, principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | DuplicateEntityException e) {
            model.addAttribute("errorMessage", e);
        }

        return "redirect:/beers";

    }

    @PostMapping("/add/toDrunkList/{beerId}")
    public String addToDrunkList(@PathVariable int beerId, Principal principal, Model model) {
        try {
            userService.addBeerToUserDrunkenList(beerId, principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | DuplicateEntityException e) {
            model.addAttribute("errorMessage", e);
        }

        return "redirect:/beers";

    }

    @PostMapping("/remove/wishlist/{beerId}")
    public String removeFromWishList(@PathVariable int beerId, Principal principal, Model model) {
        User user = mapFromPrincipalToUser(principal);
        try {
            userService.removeFromList("wishlist", user.getId(), beerId);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | DuplicateEntityException e) {
            model.addAttribute("errorMessage", e);
        }
        return "redirect:/users/profile/" + user.getUserName();
    }

    @GetMapping("/delete/{beerId}")
    public String deleteBeer(@PathVariable int beerId, Principal principal, Model model) {
        User user = mapFromPrincipalToUser(principal);
        try {
            beerService.delete(beerId, user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | DuplicateEntityException e) {
            model.addAttribute("errorMessage", e);
        }
        return "redirect:/users/profile/" + user.getUserName();
    }

    private User mapFromPrincipalToUser(Principal principal) {
        User user = null;
        try {
            user = userService.getByUsername(principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | NullPointerException e) {

        }
        return user;
    }

}
