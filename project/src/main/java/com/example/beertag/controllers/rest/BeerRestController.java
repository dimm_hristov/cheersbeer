package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.exceptions.InvalidOperationException;
import com.example.beertag.models.*;
import com.example.beertag.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private final BeerService beerService;
    private final StyleService styleService;
    private final UserService userService;
    private final OriginCountryService originCountryService;
    private final BreweryService breweryService;
    private final RatingService ratingService;

    @Autowired
    public BeerRestController(BeerService beerService, StyleService styleService, UserService userService,
                              OriginCountryService originCountryService, BreweryService breweryService, RatingService ratingService) {
        this.beerService = beerService;
        this.styleService = styleService;
        this.userService = userService;
        this.originCountryService = originCountryService;
        this.breweryService = breweryService;
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @GetMapping("/deleted")
    public List<Beer> getAllDeletedBeers() {
        try {
            return beerService.getAllDeletedBeers();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beerService.getById(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/search")
    public List<Beer> search(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Integer styleId,
            @RequestParam(required = false) Integer countryId) {
        if (name != null && !name.isBlank()) {
            return beerService.filterByName(name);
        } else if (styleId != null && styleId > 0) {
            return beerService.filterByStyle(styleId);
        } else if (countryId != null && countryId > 0) {
            return beerService.filterByCountry(countryId);
        } else {
            return new ArrayList<>();
        }
    }


    @GetMapping("sort/{filter}/{order}")
    public List<Beer> sortBy(@PathVariable String filter,
                             @PathVariable(required = false) String order) {

        switch (filter) {
            case "name":
                if (order != null && order.equals("desc")) {
                    List<Beer> beerList = beerService.sortByName();
                    Collections.reverse(beerList);
                    return beerList;
                }
                return beerService.sortByName();

            case "abv":
                if (order != null && order.equals("desc")) {
                    List<Beer> beerList = beerService.sortByAbv();
                    Collections.reverse(beerList);
                    return beerList;
                }
                return beerService.sortByAbv();

            case "rating":
                Set<Beer> setBeers = beerService.sortByRating();
                List<Beer> list = new ArrayList<>();
                list.addAll(setBeers);
                if (order != null && order.equals("asc")) {
                    Collections.reverse(list);
                }

                return list;

        }
        return getAll();
    }


    @PostMapping
    public Beer create(@Valid @RequestBody BeerDto beerDto, @RequestHeader("Authorization") String username) {
        try {
            Beer beer = new Beer();
            Beer newBeer = mapper.toBeer(beerDto, beer, styleService, originCountryService, breweryService);
            User beerCreator = userService.getByUsername(username);
            beerService.create(newBeer, beerCreator);
            return newBeer;
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Beer update(@Valid @RequestBody BeerDto beerDto, @RequestHeader("Authorization") String username) {
        try {
            Beer beer = beerService.getById(beerDto.getId());
            Beer updatedBeer = mapper.toBeer(beerDto, beer, styleService, originCountryService, breweryService);
            User user = userService.getByUsername(username);
            beerService.update(updatedBeer, user);
            return updatedBeer;
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/tags")
    public List<Tag> updatedTags(@RequestBody List<String> tags) {
        //TODO
        return null;
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader("Authorization") String username) {
        try {
            User beerCreator = userService.getByUsername(username);
            beerService.delete(id, beerCreator);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/rate/{beerId}/{rating}")
    public void rateBeer(@RequestHeader("Authorization") String username,
                         @PathVariable int beerId, @PathVariable int rating) {
        try {
            ratingService.rate(username, beerId, rating);
        } catch (EntityNotFoundException | DuplicateEntityException
                | EntityHasBeenDeletedException | InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/unrate/{beerId}")
    public void unrateBeer(@RequestHeader("Authorization") String username, @PathVariable int beerId) {
        try {
            ratingService.unrateBeer(username, beerId);
        } catch (EntityNotFoundException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/getrating/{beerId}")
    public double calculateRating(@PathVariable int beerId) {
        try {
            return ratingService.calculateRating(beerId);
        } catch (EntityNotFoundException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    @RequestMapping(value = "/addtags/{beerId}", method = RequestMethod.POST,
            consumes = "application/json")
    public Set<String> addTagsToBeer(@RequestBody TagWrapper tags, @PathVariable int beerId) {
        try {
            Set<Tag> setOfTagsToAdd = tags.getTags().stream().map(Tag::new).collect(Collectors.toSet());
            beerService.addListOfTagsToBeer(setOfTagsToAdd, beerId);
            return tags.getTags();
        } catch (EntityNotFoundException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @GetMapping("/searchByCriteria")
    public List<Beer> searchByCriteria(@RequestBody SearchBeerDto searchBeerDto) {
        return beerService.searchByCriteria(searchBeerDto.getName(), searchBeerDto.getStyleId(), searchBeerDto.getCountryId(), searchBeerDto.getTags());

    }
}
