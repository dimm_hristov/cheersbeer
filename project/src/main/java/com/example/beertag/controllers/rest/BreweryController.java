package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/breweries")
public class BreweryController {
    private final BreweryService service;

    @Autowired
    public BreweryController(BreweryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Brewery> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Object getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public Brewery create(@Valid @RequestBody Brewery brewery) {
        try {
            service.create(brewery);
            return brewery;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Brewery update(@Valid @RequestBody Brewery brewery) {
        try {
            service.update(brewery);
            return brewery;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Brewery delete(@PathVariable int id) {
        try {
            Brewery breweryToDelete = service.getById(id);
            service.delete(id);
            return breweryToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
