package com.example.beertag.exceptions;

public class EntityHasBeenDeletedException extends RuntimeException {
    public EntityHasBeenDeletedException(String message) {
        super(message);
    }
}
