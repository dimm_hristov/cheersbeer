package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.exceptions.InvalidOperationException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.BeerRepository;
import com.example.beertag.repositories.contracts.RatingRepository;
import com.example.beertag.repositories.contracts.TagRepository;
import com.example.beertag.repositories.contracts.UserRepository;
import com.example.beertag.services.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BeerServiceImpl implements BeerService {
    private final BeerRepository beerRepository;
    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final RatingRepository ratingRepository;


    @Autowired
    public BeerServiceImpl(BeerRepository repository, UserRepository userRepository,
                           TagRepository tagRepository, RatingRepository ratingRepository) {
        this.beerRepository = repository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
        this.ratingRepository = ratingRepository;

    }

    @Override
    public Beer getById(int id) {
        return beerRepository.getById(id);
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public List<Beer> getAllDeletedBeers() {
        if (beerRepository.getAllDeletedBeers().isEmpty()) {
            throw new EntityNotFoundException("There aren't any deleted beers yet");
        }
        return beerRepository.getAllDeletedBeers();
    }


    @Override
    public void create(Beer beer, User creator) {
        if (beerRepository.getAll().stream().anyMatch(b -> b.getName().equalsIgnoreCase(beer.getName()))) {
            throw new DuplicateEntityException(
                    String.format("Beer with name %s already exist", beer.getName()));
        }
        if (userRepository.getById(creator.getId()) != null) {
            beer.setCreator(creator);
        } else {
            throw new EntityNotFoundException(String.format("User with name %s doesn't exist.", creator));
        }
        beerRepository.create(beer);
    }

    @Override
    public List<Beer> filterByName(String name) {
        return beerRepository.filterByName(name);
    }

    @Override
    public List<Beer> filterByStyle(int styleId) {
        return beerRepository.filterByStyle(styleId);
    }

    @Override
    public List<Beer> filterByCountry(int countryId) {
        return beerRepository.filterByCountry(countryId);
    }

    @Override
    public List<Beer> filterByCountry(String countryName) {
        return beerRepository.filterByCountry(countryName);
    }

    @Override
    public void update(Beer beer, User user) {
        if (!beer.getCreator().getUserName().equals(user.getUserName())) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit beer with id %d", user.getUserName(), user.getId()));
        }
        beerRepository.update(beer);
    }


    @Override
    public void delete(int id, User user) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean hasRoleAdmin = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
        if (hasRoleAdmin) {
            Beer beerToDelete = beerRepository.getById(id);
            if (!beerToDelete.getCreator().getUserName().equals(user.getUserName())) {
                throw new InvalidOperationException(
                        String.format("User %s is not allowed to edit beer with name %s", user.getUserName(), beerToDelete.getName()));
            }
        }
        beerRepository.delete(id);
    }

    @Override
    public void addTagToBeer(String tagName, int beerId) {
        Beer beer = beerRepository.getById(beerId);
        Tag tag;
        if (tagRepository.getByTagName(tagName) != null) {
            tag = tagRepository.getByTagName(tagName);
        } else {
            tag = new Tag(tagName);
            tagRepository.create(tag);
        }
        if (beer.getTags().contains(tag)) {
            throw new DuplicateEntityException(String.format("Tag %s already exist", tagName));
        }
        beer.getTags().add(tag);
        beerRepository.update(beer);
    }

    @Override
    public void addListOfTagsToBeer(Set<Tag> tags, int beerId) {
        Beer beer = beerRepository.getById(beerId);
        for (Tag tag : tags) {
            if (tagRepository.getByTagName(tag.getTag()) != null) {
                tag = tagRepository.getByTagName(tag.getTag());
                beer.getTags().add(tag);
                beerRepository.update(beer);
            } else {
                tag = new Tag(tag.getTag());
                tagRepository.create(tag);
                beer.getTags().add(tag);
                beerRepository.update(beer);
            }
        }
    }

    @Override
    public List<Beer> sort(String filter, List<Beer> beers) {

        switch (filter) {
            case "name":
                beers.sort(Comparator.comparing(Beer::getName));
                break;
            case "abv":
                beers.sort(Comparator.comparing(Beer::getAbv));
                break;
            case "rating":
                beers.sort(Comparator.comparing(Beer::getRating));
                break;
        }
        return beers;
    }

    @Override
    public List<Beer> sortByName() {
        return beerRepository.sortByName();
    }

    @Override
    public List<Beer> sortByName(List<Beer> list) {
        list.sort(Comparator.comparing(Beer::getName));
        return list;
    }

    @Override
    public List<Beer> sortByAbv() {
        return beerRepository.sortByAbv();
    }

    @Override
    public Set<Beer> sortByRating() {
        Set<Beer> beersWithoutCalculatedRating = beerRepository.sortByRating();
        //map contains calculatet rating and the beerId
        Map<Double, Integer> beerMap = new TreeMap<>();
        for (Beer beer : beersWithoutCalculatedRating) {
            int beerId = beer.getId();
            double beerRating = ratingRepository.calculateRating(beer);
            beerMap.put(beerRating, beerId);
        }
        List<Beer> beersCalculatedInAscOrder = new ArrayList<>();
        Set<Beer> beerSetAscToReturn = new LinkedHashSet<>();
        beerMap.values().forEach(id -> beersCalculatedInAscOrder.add(beerRepository.getById(id)));
        Collections.reverse(beersCalculatedInAscOrder);
        for (int i = 0; i < beersCalculatedInAscOrder.size(); i++) {
            beerSetAscToReturn.add(beersCalculatedInAscOrder.get(i));
        }
        return beerSetAscToReturn;
    }

    @Override
    public List<Beer> searchByCriteria(String name, int styleId, int countryId, String tags) {
        return beerRepository.searchByCriteria(name, styleId, countryId, tags);

    }

}
