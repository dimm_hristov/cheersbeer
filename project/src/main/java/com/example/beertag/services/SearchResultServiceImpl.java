package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.repositories.SearchResultRepositoryImpl;
import com.example.beertag.services.contracts.SearchResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchResultServiceImpl implements SearchResultService {
    private final SearchResultRepositoryImpl searchResultRepository;

    @Autowired
    public SearchResultServiceImpl(SearchResultRepositoryImpl searchResultRepository) {
        this.searchResultRepository = searchResultRepository;
    }

    @Override
    public List<Beer> getList() {
        return searchResultRepository.getSearchResult();
    }

    @Override
    public List<Beer> addBeers(List<Beer> beersToAdd) {
        List<Beer> result = searchResultRepository.getSearchResult();
        result.clear();
        result.addAll(beersToAdd);
        return result;
    }

    @Override
    public String getLastSortedBy() {
        return searchResultRepository.getLastSortedBy();
    }

    @Override
    public void setLastSortedBy(String sort) {
        searchResultRepository.setLastSortedBy(sort);
    }
}
