package com.example.beertag.services;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Tag;
import com.example.beertag.repositories.contracts.StyleRepository;
import com.example.beertag.repositories.contracts.TagRepository;
import com.example.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    private final TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> getAll() {
        return repository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Tag tag) {
        if (tagExist(tag.getTag())) {
            throw new EntityNotFoundException(String.format
                    ("Tag with name %s already exist", repository.getById(tag.getId())));
        }
        repository.create(tag);
    }

    @Override
    public void createWithoutError(Tag tag) {

        repository.create(tag);
    }

    @Override
    public void update(Tag tag) {
        if (!tagExist(tag.getTag())) {
            throw new EntityNotFoundException(String.format
                    ("Tag with name %s doesn't exist", repository.getById(tag.getId())));
        }
        repository.update(tag);
    }

    @Override
    public void delete(int id) {
        if (!tagExist(id)) {
            throw new EntityNotFoundException(String.format
                    ("Tag with name %s doesn't exist", repository.getById(id)));
        }
        repository.delete(id);
    }

    @Override
    public void addTagToBeer(int tagId, int beerId) {

    }

    @Override
    public boolean tagExist(String tagName) {
        return repository.getAll()
                .stream()
                .anyMatch(s -> s.getTag().equalsIgnoreCase(tagName));
    }

    @Override
    public boolean tagExist(int id) {
        return repository.getAll()
                .stream()
                .anyMatch(s -> s.getId() == id);
    }

    @Override
    public Tag getByTagName(String tag) {
        return repository.getByTagName(tag);
    }
}
