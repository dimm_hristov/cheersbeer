package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Style;
import com.example.beertag.repositories.contracts.StyleRepository;
import com.example.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private final StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAll() {
        return styleRepository.getAll();
    }

    @Override
    public Style getById(int id) {
        return styleRepository.getById(id);
    }

    @Override
    public void create(Style style) {
        if (styleExist(style.getStyle(), styleRepository)) {
            throw new DuplicateEntityException(String.format("Style with name %s already exist", style.getStyle()));
        }
        styleRepository.create(style);
    }

    @Override
    public void update(Style style) {
        if (!styleExist(style.getId(), styleRepository)) {
            throw new EntityNotFoundException(String.format("Style with name %s doesn't exist", style.getStyle()));
        }
        styleRepository.update(style);
    }

    @Override
    public void delete(int id) {
        if (!styleExist(id, styleRepository)) {
            throw new EntityNotFoundException(String.format
                    ("Style with name %s doesn't exist", styleRepository.getById(id)));
        }
        styleRepository.delete(id);
    }

    private boolean styleExist(String styleName, StyleRepository styleRepository) {
        return styleRepository.getAll()
                .stream()
                .anyMatch(s -> s.getStyle().equalsIgnoreCase(styleName));
    }

    private boolean styleExist(int id, StyleRepository styleRepository) {
        return styleRepository.getAll()
                .stream()
                .anyMatch(s -> s.getId() == id);
    }
}
