package com.example.beertag.services.contracts;

import com.example.beertag.models.Style;

import java.util.List;

public interface StyleService {

    List<Style> getAll();

    Style getById(int id);

    void create(Style style);

    void update(Style style);

    void delete(int id);
}
