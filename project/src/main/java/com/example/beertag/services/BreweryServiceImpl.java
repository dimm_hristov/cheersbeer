package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.repositories.contracts.BreweryRepository;
import com.example.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {
    private final BreweryRepository repository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Brewery getByBreweryName(String name) {
        return repository.getByBreweryName(name);
    }

    @Override
    public void create(Brewery brewery) {
        if (breweryExist(brewery.getBrewery(), repository)) {
            throw new DuplicateEntityException(String.format("Brewery with name %s already exist", brewery.getBrewery()));
        }
        repository.create(brewery);
    }

    @Override
    public void update(Brewery brewery) {
        if (!breweryExist(brewery.getId(), repository)) {
            throw new EntityNotFoundException(String.format("Brewery with name %s doesn't exist", brewery.getBrewery()));
        }
        repository.update(brewery);
    }

    @Override
    public void delete(int id) {
        if (!breweryExist(id, repository)) {
            throw new EntityNotFoundException(String.format
                    ("Brewery with name %s doesn't exist", repository.getById(id)));
        }
        repository.delete(id);
    }

    private boolean breweryExist(String breweryName, BreweryRepository repository) {
        return repository.getAll()
                .stream()
                .anyMatch(b -> b.getBrewery().equalsIgnoreCase(breweryName));
    }

    private boolean breweryExist(int id, BreweryRepository repository) {
        return repository.getAll()
                .stream()
                .anyMatch(b -> b.getId() == id);
    }
}
